package com.hashemi;

/**
 * @author Parisa Hashemi
 * @version 1.0.0
 * @since 1.0.0
 *
 * Creation Date: 2016/06/11
 */
public interface Forwarder {
    void forward();
}
