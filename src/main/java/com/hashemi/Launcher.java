package com.hashemi;

import javax.swing.*;
import java.awt.*;

/**
 * @author Parisa Hashemi
 * @version 1.0.0
 * @since 1.0.0
 *
 * Creation Date: 2016/06/11
 */
public class Launcher {
    public static void main(String... args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JFrame frame = new JFrame("Music Player");
                Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                frame.setSize(500, 300);
                frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
                frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                frame.setResizable(false);
                frame.setVisible(true);
                JPanel panel = new PlayerPanel();
                frame.add(panel);
            }
        });
    }
}
