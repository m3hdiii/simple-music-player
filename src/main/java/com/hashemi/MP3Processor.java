package com.hashemi;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @author Parisa Hashemi
 * @version 1.0.0
 * @since 1.0.0
 *
 * Creation Date: 2016/06/11
 */
public class MP3Processor {

    private FileInputStream fis;
    private Player player;
    private long pauseLocation;
    private long songTotalLength;
    private String filePath;
    private Forwarder forwarder;

    public MP3Processor(Forwarder forwarder) {
        this.forwarder = forwarder;
    }


    public void stop() {
        if (player != null) {
            player.close();
            pauseLocation = 0;
            songTotalLength = 0;
        }
    }


    public void pause() {
        if (player != null) {
            try {
                pauseLocation = fis.available();
                player.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void play(String path) {
        if (pauseLocation != 0) {
            resume();
            return;
        }

        if (player != null) {
            player.close();
        }


        try {
            fis = new FileInputStream(path);
            player = new Player(fis);
            songTotalLength = fis.available();
            this.filePath = path;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JavaLayerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        new Thread() {
            @Override
            public void run() {
                doPlay();
            }
        }.start();

    }

    public void resume() {
        if (player != null) {
            player.close();
        }

        try {
            fis = new FileInputStream(filePath);
            player = new Player(fis);

            fis.skip(songTotalLength - pauseLocation);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (JavaLayerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        pauseLocation = 0;

        new Thread() {
            @Override
            public void run() {
                doPlay();
            }
        }.start();
    }

    private void doPlay() {
        try {
            player.play();
            if (player.isComplete()) {
                if (!PlayerPanel.flag) {
                    if (forwarder != null) {
                        forwarder.forward();
                    }
                }else{
                    player.close();
                    play(filePath);
                }
            }
        } catch (JavaLayerException e) {
            e.printStackTrace();
        }
    }
}
